import XCTest
@testable import FyndTrueSDK

final class FyndTrueSDKTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(FyndTrueSDK().text, "Hello, World!")
    }
}
